#include "stdafx.h"
#include "DrawVertex.h"


void DrawVertex::Initialize()
{
	shader = new Shader(L"004_Quad.fx");

	// >> : vertex buffer
	vertexCount = (width + 1) * (height + 1);
	vertices = new Vectex[vertexCount];

	for (UINT z = 0; z <= height; z++)
	{
		for (UINT x = 0; x <= width; x++)
		{
			UINT index = (width + 1) * z + x;
			vertices[index].Position.x = (float)x;
			vertices[index].Position.y = 0.0f;
			vertices[index].Position.z = (float)z;

		}
	}


	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(D3D11_BUFFER_DESC));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex) * vertexCount;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		D3D11_SUBRESOURCE_DATA subResource = { 0 };
		subResource.pSysMem = vertices;

		Check(D3D::GetDevice()->CreateBuffer(&desc,
			&subResource,
			&vertexBuffer));
	}


	// >> : index buffer
	{
		indexCount = width * height * 6;
		indices = new UINT[indexCount];

		UINT index = 0;
		for (UINT z = 0; z < height; z++)
		{
			for (UINT x = 0; x < width; x++)
			{
				indics[index + 0] = (width + 1) * (z + 0) + x;
				indics[index + 1] = (width + 1) * (z + 1) + x;
				indics[index + 2] = (width + 1) * (z + 0) + x + 1;
				indics[index + 3] = (width + 1) * (z + 0) + x + 1;
				indics[index + 4] = (width + 1) * (z + 1) + x;
				indics[index + 5] = (width + 1) * (z + 1) + x + 1;
				index += 6;
			}
		}

		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(D3D11_BUFFER_DESC));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(UINT) * indexCount;
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;

		D3D11_SUBRESOURCE_DATA subResource = { 0 };
		subResource.pSysMem = indices;

		Check(D3D::GetDevice()->CreateBuffer(&desc,
			&subResource,
			&IndexBuffer));
		// << : 
	}
	color = Color(0, 0, 0, 1);

}

void DrawVertex::Destroy()
{
	SafeDelete(shader);
	SafeRelease(vertexBuffer);
	SafeRelease(IndexBuffer)
}

void DrawVertex::Update()
{
	ImGui::ColorEdit3("Color", (float*)&color);
	shader->AsVector("Color")->SetFloatVector(color);

	Matrix view = Context::Get()->View();
	shader->AsMatrix("View")->SetMatrix(view);

	Matrix projection = Context::Get()->Projection();
	shader->AsMatrix("Projection")->SetMatrix(projection);
}

void DrawVertex::Render()
{
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	D3D::GetDC()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	D3D::GetDC()->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R32_UINT,
		0);
	
	D3D::GetDC()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	static bool b = false;
	ImGui::Checkbox("WireFrame", &b);
	//shader->Draw(0, (b ? 0 : 1), 6);
	shader->DrawIndexed(0, (b ? 0 : 1), 6);
	{
		static Vector3 position(1, 0, 0);
		Matrix world;
		D3DXMatrixIdentity(&world);
		D3DXMatrixScaling(&world, 10, 1, 10);

		shader->AsVector("Color")->SetFloatVector(color);
		shader->AsMatrix("World")->SetMatrix(world);
		shader->Draw(0,(b ? 0 : 1), indexCount);
	}
}



/*
 *	/*
	ImGui::ColorEdit3("Color", (float*)&color);


	ImGui::InputInt("Number",(int*)&number);
	number %= 6;

	if (Keyboard::Get()->Press(VK_LEFT))
	{
		vertices[number].Position.x -= 2.0f * Time::Delta();
	}
	else 	if (Keyboard::Get()->Press(VK_RIGHT))
	{
		vertices[number].Position.x += 2.0f * Time::Delta();
	}

	if (Keyboard::Get()->Press(VK_DOWN))
	{
		vertices[number].Position.y -= 2.0f * Time::Delta();
	}
	else 	if (Keyboard::Get()->Press(VK_UP))
	{
		vertices[number].Position.y += 2.0f * Time::Delta();
	}
	*/


	/*
	Matrix world;
	D3DXMatrixIdentity(&world);

	static Vector3 position(0, 0, 0);
	{
		if (Keyboard::Get()->Press(VK_LEFT))
		{
			position.x -= 1.0f * Time::Delta();
		}
		else if (Keyboard::Get()->Press(VK_RIGHT))
		{
			position.x += 1.0f * Time::Delta();
		}

		D3DXMatrixTranslation(&world, position.x, position.y, position.z);
	}

	shader->AsMatrix("World")->SetMatrix(world);

	Matrix view = Context::Get()->View();
	shader->AsMatrix("View")->SetMatrix(view);

	Matrix projection = Context::Get()->Projection();
	shader->AsMatrix("Projection")->SetMatrix(projection);


	D3D::GetDC()->UpdateSubresource(vertexBuffer, 0, NULL,
		vertices, sizeof(Vertex) * 6, 0);
*/