#pragma once

#include "Systems/IExecute.h"

class DrawVertex : public IExecute
{
private:
	Shader* shader;

	UINT	width	= 256;
	UINT	height	= 256;

	UINT	vertexCount;
	Vertex vertices[4];
	ID3D11Buffer* vertexBuffer;
	
	UINT indexCount;
	UINT* indices;
	ID3D11Buffer* IndexBuffer;
	
	Color color;

public:
	void Initialize() override;
	void Ready() override {};
	void Destroy() override;
	void Update() override;
	void PreRender() override {};
	void Render() override;
	void PostRender() override {};
	void ResizeScreen() override {};
};

